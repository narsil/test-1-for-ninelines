// System
const webpack = require('webpack')
const fs = require('fs')
const path = require('path')

// Common
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
// const { VueLoaderPlugin } = require('vue-loader')
const ESLintPlugin = require('eslint-webpack-plugin')
const PrettierPlugin = require('prettier-webpack-plugin')
const StylelintPlugin = require('stylelint-webpack-plugin')

// Development
const address = require('ip').address

// Production
const HtmlBeautifyPlugin = require('@nurminen/html-beautify-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')

// User
const paths = require('./webpack/paths')
const helpers = require('./webpack/helpers')

const pugTemplates = []
const srcll = fs.readdirSync(paths.dirSrcPug)
srcll.forEach(s => s.endsWith('.pug') && pugTemplates.push(s))


const webpackConfig = {
  mode: process.env.NODE_ENV,
  entry: {
    main: paths.dirSrcJs + '/main.js',
    // vue: paths.dirSrc + '/vue/vue.js'
  },
  output: {
    path: paths.dirDist,
    filename: 'js/[name].bundle.js',
    publicPath: '',
  },
  // resolve: {
  //   alias: {
  //     '@root': paths.dirSrcJs,
  //     '@vue': paths.dirSrc + '/vue',
  //   }
  // },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    useBuiltIns: 'entry',
                    corejs: "3.10"
                  }
                ],
                // '@vue/babel-preset-jsx'
              ],
              plugins: [
                ['@babel/plugin-proposal-class-properties', { 'loose': true }],
                ['@babel/plugin-proposal-optional-chaining'],
                // ['@vue/babel-plugin-jsx'],
              ]
            }
          }
        ]
      },
      {
        test: /\.css$/,
        use: helpers.styleLoaders([], process.env.NODE_ENV)
      },
      {
        test: /\.scss$/,
        use: helpers.styleLoaders([{
          loader: 'sass-loader',
          options: {
            sourceMap: process.env.NODE_ENV === 'development' ? true : false,
          },
        }], process.env.NODE_ENV)
      },
      {
        test: /\.pug$/,
        use: ['raw-loader', 'pug-html-loader']
      },
      // {
      //   test: /\.vue$/,
      //   loader: 'vue-loader'
      // },
      {
        test: /\.svg$/,
        loader: 'svg-url-loader'
      }
    ]
  },
  plugins: [
    // new webpack.DefinePlugin({
    //   __VUE_OPTIONS_API__: true,
    //   __VUE_PROD_DEVTOOLS__: false,
    // }),
    ...pugTemplates.map(templateName => new HtmlWebpackPlugin({
      inject: true,
      template: `./src/pug/${templateName}`,
      filename: path.join(paths.dirDist, templateName.replace('.pug', '.html')),
      minify: false,
      // alwaysWriteToDisk: true,
    })),
    new MiniCssExtractPlugin({
      filename: 'css/[name].css'
    }),
    new CopyWebpackPlugin({
      patterns: [
        { from:'src/assets', to:'assets', noErrorOnMissing: true },
      ]
    }),
    // new VueLoaderPlugin(),
    new ESLintPlugin({
      files: ['.', 'src', 'config'],
      formatter: 'table',
    }),
    new StylelintPlugin({
      fix: process.env.NODE_ENV === 'development' ? true : false,
    }),
  ],
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
    // runtimeChunk: 'single',
    // splitChunks: {
    //   chunks: 'all',
    //   maxInitialRequests: Infinity,
    //   minSize: 0,
    //   cacheGroups: {
    //     vendor: {
    //       test: /[\\/]node_modules[\\/]/,
    //       name(module) {
    //         const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
            
    //         if (packageName.includes('vue')) {
    //           return 'vendors.vue';
    //         }

    //         return 'vendors.common';
    //       },
    //     },
    //   },
    // },
  }
}

if (process.env.NODE_ENV === 'development') {
  webpackConfig.target = 'web'
  webpackConfig.plugins.push(
    new PrettierPlugin({
      printWidth: 120,
      tabWidth: 2,
      useTabs: false,
      semi: false,
      singleQuote: true,
      quoteProps: 'as-needed',
      jsxSingleQuote: false,
      trailingComma: 'es5',
      bracketSpacing: true,
      jsxBracketSameLine: false,
      arrowParens: 'avoid',
      requirePragma: false,
      insertPragma: false,
      encoding: 'utf-8',
      extensions: [ '.js', '.scss', '.vue' ],
    })
  )
  webpackConfig.devtool = 'source-map'
  webpackConfig.plugins.push(new webpack.HotModuleReplacementPlugin())
  // wds 4-beta
  // object { bonjour?, client?, compress?, dev?, firewall?, headers?, historyApiFallback?, host?, hot?, http2?, https?, liveReload?, onAfterSetupMiddleware?, onBeforeSetupMiddleware?, onListening?, open?, port?, proxy?, public?, setupExitSignals?, static?, transportMode?, watchFiles? }
  webpackConfig.devServer = {
    historyApiFallback: true,
    contentBase: paths.dirDist,
    compress: true,
    progress: true,
    quiet: false,
    // hot: true,
    lazy: false,
    host: address(),
    port: 3000,
    clientLogLevel: 'error',
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    open: true,
    // writeToDisk: true,
  }
}

if (process.env.NODE_ENV === 'production') {
  webpackConfig.plugins.push(new HtmlBeautifyPlugin({
    config: {
      html: {
        end_with_newline: false,
        indent_size: 2,
        indent_with_tabs: false,
        indent_inner_html: false,
        preserve_newlines: false,
        wrap_line_length: 0,
        inline: [],
        unformatted: [],
      }
    },
  }))
  webpackConfig.plugins.push(new BundleAnalyzerPlugin())
  webpackConfig.optimization.minimize = true
  webpackConfig.optimization.minimizer = [
    new TerserPlugin(),
    new CssMinimizerPlugin()
  ]
}

module.exports = webpackConfig
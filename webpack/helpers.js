const { loader } = require('mini-css-extract-plugin')

const styleLoaders = (loaders = [], mode = 'development') => {
  return [
    {
      loader: loader,
    },
    {
      loader: 'css-loader',
      options: {
        sourceMap: mode === 'development' ? true : false,
        url: false
      }
    },
    {
      loader: 'postcss-loader'
    }
  ].concat(loaders)
}

module.exports = {
  styleLoaders
}
import 'tailwindcss/tailwind.css'
import '../scss/main.scss'

try {
  const $checkboxInputs = document.querySelectorAll('.input-checkbox__input')
  const checkboxInputsCount = $checkboxInputs.length
  const $arrow = document.querySelector('.result-scale__arrow')
  const $number = document.querySelector('.profile__result-number')
  let currentScore = 0

  function animateValue(node, start, end, duration) {
    if (start === end) return

    const range = end - start
    let current = start
    const increment = end > start ? 1 : -1
    const stepTime = Math.abs(Math.floor(duration / range))
    const timer = setInterval(() => {
      current += increment
      node.textContent = current
      if (current === end) {
        clearInterval(timer)
      }
    }, stepTime)
  }

  function checkboxHandler() {
    const checked = Array.from($checkboxInputs).reduce((acc, input) => (acc += input.checked), 0)

    $arrow.style.transform = `rotateZ(${180 * (checked / checkboxInputsCount)}deg)`
    animateValue($number, currentScore, checked * 20, 300)
    currentScore = checked * 20
  }

  $checkboxInputs.forEach(check => check.addEventListener('change', checkboxHandler))
} catch (error) {}
